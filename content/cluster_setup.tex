% Cluster Setup erklären, wie in Präsentation
\section{Cluster Setup}\label{chapter:cluster_setup}

We have already seen the typical container workflow, for example with Docker, in \autoref{fig:docker_ecosystem_overview}. Most of these steps involve first building the container image from source and then pushing it to a remote registry to distribute that very image. After it has been distributed it can be pulled down again by other clients by specifying the \lstinline{docker pull} command. From a security perspective this is the point in time where it can get dangerous since we are basically pulling a binary from the internet and execute it afterwards. As mentioned before, we want to secure this pipeline by preventing vulnerable containers from running in production.
The base architecture for this idea is depicted in \autoref{fig:base_arch}.

\image{\textwidth}{arch_design}{Underlying architecture to prevent the deployment of insecure container images.}{fig:base_arch}
%
Whenever an image gets built by a developer it first has to pass the tests that are located in the \enquote{Security Suite} Kubernetes namespace which is a collection of one or more security components enforcing the desired security policies. If everything went well, the image would be pushed to the private registry where it can be pulled again.

Since we want to detect vulnerabilities present in container images a tool is needed that is able to expose such CVEs by static analysis of the layers an image is composed of. Refer to \autoref{chapter:static_analysis_comparison} for more information.

% Hier Vergleich CVE Tools
\subsection{Comparison of Static Analysis Tools for Container Images}\label{chapter:static_analysis_comparison}
A short comparison of tools that conduct static analysis of container images follows. Only tools that use some sort of persistence are considered.

\subsubsection{Clair}
Clair \cite{clair} from Red Hat is an API-driven static analysis tool for container images which ingests its CVE database from a variety of sources such as the Debian Security Bug Tracker, the Ubuntu CVE Tracker, Red Hat Security Data, and many more.
It currently supports the scanning of appc and Docker images; It is written in the Go programming language. It is used by Quay.io \cite{quayio} which is a public container registry alternative to Docker Hub. The vulnerability data can be updated from upstream within a fixed interval and is afterwards correlated with the indexed contents of container images in order to produce lists of vulnerabilities that threaten an image. When the vulnerability metadata changes upstream, the previous and new state of the vulnerability along with the affected images can be sent via webhook to a configured endpoint \cite{clair}.

\subsubsection{Anchore Engine}
Anchore Engine \cite{anchore} is a service for analyzing Docker images which is basically CVE-based security vulnerability reporting just like Clair. However, in addition, it allows to apply user-defined acceptance policies in order to provide automated container image validation and certification \cite{anchoregithub}. Anchore Engine is written in Python. From my personal anecdotal experience it admittedly tends to perform security scanning of the same container image significantly slower than Clair.

\subsubsection{Dagda}
Dagda \cite{dagda} is another tool for the static analysis of container images. However, it does not only yield CVEs but also viruses, trojans, malware, and other malicious threats. In order to facilitate the latter, Dagda uses ClamAV \cite{clamav} as an antivirus engine to detect trojans, viruses, and other malware present in container images. However, Dagda also offers to monitor the Docker daemon and running containers in order to detect anomalous activities. This is achieved since it offers integration with Falco \cite{falco} which is a separate tool for container runtime security. It is also written in Python. This project sounds interesting since it promises to cover more features than Anchore Engine and Clair, in particular runtime security. However, due to time constraints the application of this tool could not be examined.

Hence, I decided to deploy Clair, the open source project from Red Hat, in the Kubernetes cluster. The primary reason for that decision is the fact that it is much faster than its competitors.
However, Clair alone only exposes its REST API for conducting security scans. Since we can not expect users to manually craft their HTTP requests to Clair, a form of integration as a frontend between the default docker registry, sometimes called distribution, and Clair as a security scanner is wanted. Additional desired features, among other things, are:
\begin{itemize}
    \item Authentication
    \item Access Control
    \item Enforcement of security policies by setting a vulnerability threshold
\end{itemize}
Fortunately, there is an open source project from the \nomenclature{CNCF}{Cloud Native Computing Foundation} CNCF for these requirements: Harbor \cite{goharbor} which has the goal of extending the Docker registry with extra functionality.
Here is a non-exhaustive list of features:
\begin{itemize}
    \item \textbf{Cloud native registry}: Supports management of container images and Kubernetes Helm charts
    \item \textbf{Role based access control}: Repositories are managed in projects which provides configuration of access control
    \item \textbf{Vulnerability scanning}: Clair is used as the standard security scanner, other security scanner can be integrated if desired
    \item \textbf{Security threshold setting}: Setting the maximum allowed vulnerability level for projects
    \item \textbf{LDAP/OIDC support}: External user authentication possible, LDAP groups can be integrated in Harbor
    \item \textbf{Image signing}: Signing and authenticity checking of images possible with Notary
    \item \textbf{GUI support}
\end{itemize}

\image{\textwidth}{k3s_cluster_setup}{Illustration of the deploye components in the Kubernets cluster.}{fig:k3s_cluster_setup}

Refer to \autoref{fig:k3s_cluster_setup} for an overview of the deployed components in the Kubernetes testing cluster. The security suite namespace consists at the moment of one Harbor deployment with its required microservices such as the database, the portal, the registry, and Clair. Monitoring of the whole cluster is realized with the Prometheus Operator which is a project for monitoring a complete Kubernetes cluster via Prometheus. Prometheus is the defacto standard monitoring solution for Kubernetes and is a pull-based metrics scraper, see \autoref{chapter:prometheus} for more information. Note that some changes to Prometheus Operator were needed since k3s replaced some components of regular k8s. Grafana is the deployed dashboard for visualization of the Prometheus metrics.

All of the frontend services, namely the UI, portal, notary, grafana, prometheus, are exposed by the default Ingress controller traefik \cite{traefik} via a load balancer which acts basically as a reverse proxy and redirects the specific requests to the specific backend services. The Ingress controller also manages handling of the TLS certificates of the different services. In the default configuration these are untrusted, self-signed certificates, but can be replaced by trusted ones by Let's Encrypt for example.

With the configured components in the cluster, the usual container deployment workflow has changed; The new one is depicted in \autoref{fig:container_deploy_wf}.

\image{\textwidth}{deployment_workflow}{Outline of the new container deployment workflow.}{fig:container_deploy_wf}

% TODO: hier Infos über Prometheus
\subsection{About Prometheus}\label{chapter:prometheus}
Prometheus \cite{prometheus} is a CNCF graduated, open-source monitoring and alerting solution, primarily intended for monitoring entire Kubernetes cluster and thus it is the defacto default monitoring solution for Kubernetes.
The architecture of Prometheus is illustrated in \autoref{fig:prom_arch}. It can be seen that Prometheus is a pull based monitoring solution, i.e. it expects HTTP endpoints on the monitored servers where it can scrape its data from.

\image{\textwidth}{prom_arch}{The architecture of Prometheus and some of its ecosystem components \cite{prometheusdocs}.}{fig:prom_arch}

Of course this means that the jobs for exposing the metrics, so-called exporters, have to be provided by oneself. However, there are already many Prometheus exporters available that can be used in production, or -- if customization for one application is needed -- there exist libraries for creating custom exporters.
In addition, Prometheus allows to apply specific alerting rules over any defined metric -- if the alerting rules are triggered when the specified conditions occur, the triggered alerts are displayed on the Prometheus frontend and are sent via the configured notification channels, e.g. Email, Slack, HipChat, etc.
