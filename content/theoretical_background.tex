\section{Theoretical Background}\label{chapter:theoretical_background}

\subsection{Security Threats Regarding Docker}

Docker provides many benefits to developers; A major one is easily allowing developers to package an application with all its dependencies into a single, shippable unit which can be quickly deployed in a \textit{container}, see \autoref{fig:docker_overview} for the runtime model of Docker.

\image{.4\textwidth}{docker_overview}{The docker runtime application model \cite{10ThingstoKnowAboutDocker}. Note that the Docker daemon does \textbf{not} act like a hypervisor but only handles orchestration of containers. The host libraries are directly shared with each container.}{fig:docker_overview}

However, like all virtualized environments (and naturally all software that includes bugs and vulnerabilities), this technology comes with security threats \& concerns that must appropriately dealt with. In virtualization technologies this is especially important since resources on a host are shared among multiple guests.
To name a broad example, a major layer that can be attacked is the \textit{Virtualization layer} where the target is the hypervisor/docker daemon (via VM escaping, Management interface access, VM sprawl attacks, denial of service, etc.) and the goal is to take control of it and thereby be able to control and access the hosted containers on it \cite[p. 46:5]{Sgandurra:2016:EAT:2856149.2856126}.

% SHORT OVERVIEW OF DOCKER ECOSYSTEM
\autoref{fig:docker_ecosystem_overview} shows an overview of all usually involved components in the Docker ecosystem.
The Docker image specification process is usually done using \textit{Dockerfiles} in which declaratively all commands (similar to unix/linux commands) to arrive at the final image are sequentially listed. The Docker daemon/engine implements this specification. The resulting images can be uploaded to registries such as the Docker Hub or self-hosted registries that distribute these images to the endusers. The enduser fetches the code of the image by specifying the very image with the  \texttt{docker pull} command; The image is then locally built on the host via the Docker daemon and a container using that image can be instantiated and executed.

\image{.6\textwidth}{docker_ecosystem_overview}{The Docker ecosystem \cite{zerouali2019relation}. $(a)$ Image building workflow. $(b)$ Docker image repositories. $(c)$ The container build process.}{fig:docker_ecosystem_overview}

The following paragraphs aim to give an overview of some security threats the usage of Docker can entail, mainly based on the aspects mentioned in \cite{combe2016docker}.

\subsubsection{Isolation}
Docker achieves its isolation feature by relying on Linux kernel features such as namespaces, enabled by default, and cgroups which are by default disabled and hence must be enabled on a per-container basis. One flaw is that all containers share the same network bridge which makes them vulnerable to potential \nomenclature{ARP}{Address Resolution Protocol} ARP resolution attacks between containers on the same host \cite{combe2016docker}.

\subsubsection{Host Hardening}
Security-related limitation constraints (e.g. limited access to filesystem and network) are imposed on containers through technologies like SELinux, AppArmor, and Seccomp which provide profiles which contain the security configuration per container. However, the default profiles are generic and thus provide full access to the filesystem, network, and all capabilities of Docker containers \cite{combe2016docker} which comes with the consequence that containers are not protected from other containers.

\subsubsection{Insecure Local Configuration}
Like with all software, an insecure local configuration of Docker can result in serious security problems.
The following security concerns are especially important when used with third-party, external containers \cite{combe2016docker}:
\begin{itemize}
    \item Giving containers extended access to the host, e.g. the \texttt{-net=host} does not separate the network between host and container
    \item Mounting sensitive host directories into containers
    \item disabled cgroups
    \item Extended permissions of the Docker control socket
\end{itemize}

\subsubsection{Image Distribution Vulnerabilities}
Container images can be distributed through the Docker Hub registry which makes it possible for developers to use already existing images for their application purposes if the image suits their needs. Naturally, this is a major source of vulnerabilities since anyone can upload images; Blindly executing external images as containers can lead, for instance, to the execution of malicious code. These vulnerabilities are almost identical to those found in classic package managers \cite{combe2016docker}. Having an automated deployment pipeline hooked up to an external code repository can also lead to security problems. These pipelines are setup in such a way that new code arriving in the code repository automatically gets pulled, built, and deployed on a remote Docker host. Each client in this pipeline has to be fully trusted since it influences which code lands into production. That means for example, that a compromised GitHub account could lead to the execution of malicious code on a large set of production machines within minutes since they are all watching the same repository \cite{combe2016docker}.

\nomenclature{VM}{Virtual Machine}
\subsubsection{Using Docker as a VM Replacement}
Container solutions such as Docker do not want to achieve the same objectives as classic VMs. More specifically, the Docker developers themselves propose a \textit{microservice architecture} -- i.e. each container must contain only a single service in a single process which later can be composed to form a coherent system; Hence, containers contain much less things as a full blown VM and therefore should not be used in the way of packaging an application into a single container since it dramatically increases the attack surface of the container \cite{combe2016docker}.

\subsection{Vulnerabilities and Bugs in Outdated Docker Containers}
In \cite{zerouali2019relation}, researchers investigated the relation between outdated Docker containers, severity vulnerabilities, and bugs in which they proposed a method to assess how outdated, vulnerable, and buggy Docker images really are with respect to the latest available releases of the packages they include. This method is based on the concept of \textit{technical lag} which estimates the difference between the software deployed and the most recent version of this software (in terms of novelty, vulnerabilities, bugs) \cite{zerouali2019relation}. They conducted an empirical study that measures the technical lag for over 7000 official and unofficial Docker hub images based on Debian Linux.
The structure of the Docker container package analysis procedure is illustrated in \autoref{fig:docker_analysis_process} which comprises the following steps \cite{zerouali2019relation}:

\subsubsection{Defining the Image Base Set}
This step simply identifies which images in the Docker Hub are Debian base images for all currently supported versions, namely \textit{Testing}, \textit{Stable}, \textit{Oldstable}.
\subsubsection{Identification of Docker Hub Images in the Data Set}
The next step fulfills the job of identifying all those images that should be included in the data set, i.e. which images are derived from the base images. This can be done by simply checking if an image includes the parent layer of a debian base image, i.e. the commit hash of a layer matches with one of those that have been defined in the step before.
\subsubsection{Analysis of Image in the Data Set}
Here, all installed packages are matched to a historical archive of all Debian packages, i.e. the metadata such as name and version of the source package are tracked from the binary packages.
\subsubsection{Identification of Bugs and Vulnerabilities}
This whole step is based on a historical database with such details for all Debian packages (formed from different data sources such as the CVE database, National Vulnerability Database, etc.). Since the Debian Security Bug Tracker forms the status of known vulnerabilities for each package at source level, vulnerabilities can be linked to the packages and it can be decided if the vulnerability is still open or has been fixed in a more recent version than the one installed in the image \cite{zerouali2019relation}. A similar process is conducted for the identification of bugs in packages, using the \textit{Ultimate Debian Database} as a source for known bugs.

\image{.6\textwidth}{docker_container_package_analysis_process}{The Docker container package analysis process \cite{zerouali2019relation}.}{fig:docker_analysis_process}

The whole data extraction part was done using the tool \textit{ConPan}\footnote{\url{https://github.com/neglectos/ConPan}} that is able to inspect Docker containers and extract their installed packages to be processed for further analysis.
As this overall analysis is an automated process, it seems to be useful to include this automated measurement of \textit{technical lag} into Docker image registries or other systems in order to offer this very information to developers/deployers so they can get feedback about the security status of used images or know when they should update their image.

\subsection{Kubernetes -- An Open Source Container Orchestration System}

Kubernetes is an open source container orchestration system, i.e. it allows developers to easily deploy and manage containerized applications on many different, distributed worker nodes. You could say that Kubernetes abstracts away the actual hardware of your whole computing infrastructure and exposes it instead as a single platform for deploying and running apps, see \autoref{fig:kubernetes_single_platform}, enabling developers to configure and deploy their applications without help from sysadmins \cite[p. 39, 48]{kubernetesinaction}.

\image{.8\textwidth}{kubernetes_single_platform}{Kubernetes as a single deployment platform \cite[p. 49]{kubernetesinaction}.}{fig:kubernetes_single_platform}

\subsubsection{The Architecture of a Cluster}

A Kubernetes \textit{cluster} denotes the entirety of nodes (typically servers) that are available in your infrastructure. Nodes are split into two types \cite[p. 50]{kubernetesinaction}:
\begin{itemize}
    \item The \textit{master} node which manages the whole Kubernetes system.
    \item \textit{Worker} nodes on which the deployed applications are running on.
\end{itemize}

These two types of nodes run different components of the Kubernetes system, see \autoref{fig:kubernetes_cluster_components}.

\image{.8\textwidth}{kubernetes_cluster_components}{The components of a Kubernets cluster \cite[p. 50]{kubernetesinaction}.}{fig:kubernetes_cluster_components}

The \textit{control plane} is what controls the cluster, the components can also be run on other nodes for better availability \cite[p. 50]{kubernetesinaction}, and consists of:
\begin{itemize}
    \item The \textit{API-Server} which is the central component for communication with other components.
    \item The \textit{Scheduler} which schedules apps to worker nodes.
    \item The \textit{Controller Manager} which conducts cluster-related functions, such as keeping track of worker nodes, replicating components, handling failures, etc.
    \item \textit{etc} which stores the global cluster configuration persistently.
\end{itemize}
The \textit{worker nodes}, as already mentioned, handle the job of running the containerized applications and consist of the following components \cite[p. 51]{kubernetesinaction}:
\begin{itemize}
    \item The \textit{Container} runtime, such as Docker, rkt, etc.
    \item The \textit{Kubelet} which communicates with the Kubernetes API-Server.
    \item The \textit{kube-proxy} which handles the job of load-balancing network traffic between application components.
\end{itemize}

\subsubsection{Running Applications}
One important aspect to know is how to instruct Kubernetes to run your application. This workflow usually consists of the following steps \cite[p. 51]{kubernetesinaction}:
\begin{itemize}
    \item Package your application into one or more container images.
    \item Deploy those specified images to a container registry.
    \item Upload an application description of your app to the Kubernetes API-Server. This description specifies the required images, the relationship of the single components, the amount of \textit{replicas} of each component to run, etc.
\end{itemize}
The whole workflow is more precisely depicted in \autoref{fig:kubernetes_application_deployment_process}.

\image{\textwidth}{kubernetes_application_deployment_process}{Overview of the Kubernetes application deployment process \cite[p. 52]{kubernetesinaction}.}{fig:kubernetes_application_deployment_process}

The app description lists in this case four containers which are grouped into three sets, called \textit{Pods}. If the Pod specifies more than one container, like in the last case in the figure, that means that these containers need to run \textit{co-located} which means that they are not isolated from each other. The number next to each pod specifies the number of replicas of each pod that should run in parallel. After submitting the description it will schedule the specified number of replicas of each pod to the available worker nodes, the kubelets on those nodes then instruct the container runtime, e.g. Docker, to pull the specified container images from the registry and instantiate them (to run the containers) \cite[p. 52]{kubernetesinaction}. After deployment (while the application is typically running), Kubernetes takes care that the specified containers are running with the specified amount of replicas, e.g. restarting terminated services automatically or handling node failure by moving to another node.

Some of the advantages of using Kubernets include \cite[p. 53-55]{kubernetesinaction}:
\begin{itemize}
    \item No need anymore for a separate team for deploying applications.
    \item Simplified application deployment.
    \item Better hardware utilization.
    \item Health checking and self-healing.
    \item Automatic scaling, especially if Kubernetes runs on Cloud infrastructure which means that additional nodes for increased application load can be requested by simple API calls.
    \item Simplified application development due to the fact that applications run in the same environment during development and production.
\end{itemize}
